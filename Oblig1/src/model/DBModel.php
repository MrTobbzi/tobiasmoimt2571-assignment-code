<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create| PDO connection
			$this->db = new PDO('mysql:host=localhost;dbname=assignement_1', 'root', '');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		
		
		try{
			$booklist = array();
			$sth = $this->db->prepare("SELECT * FROM book");
			$sth->execute();
			$books = $sth->fetchAll(PDO::FETCH_OBJ);
			if($books){
				foreach($books as $key=>$book){
					$booklist[$key] = new Book($book->title, $book->author, $book->description, $book->id);
				}
				return $booklist;
			}
		}catch(PDOException $e){
			echo "Feil ved henting av bøkene: ". $e->getMessage();
			$view = new ErrorView();
			$view->create();
			return $booklist = null;
		}
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		try{
			$book = null;
			$sth = $this->db->prepare("SELECT * FROM book WHERE id = :id");
			$sth->bindValue(':id', $id, PDO::PARAM_STR);
			$sth->execute();
			$k = $sth->fetch(PDO::FETCH_OBJ);
			$book = new Book($k->title, $k->author, $k->description, $k->id);
			
			return $book;
			
		}catch(PDOException $e){
			echo "Feil ved henting av ID: ". $e->getMessage();
			$view = new ErrorView();
			$view->create();
			return $book = null;
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try{
			if(empty($book->title) || empty($book->author)){
				$view = new ErrorView();
				$view->create();
				
			}else{
				$sth = $this->db->prepare('INSERT INTO Book(title, author, description) VALUES(:t,:a,:d)');
				$sth->bindValue(':t', $book->title, PDO::PARAM_STR);
				$sth->bindValue(':a', $book->author, PDO::PARAM_STR);
				$sth->bindValue(':d', $book->description, PDO::PARAM_STR);
				$sth->execute();
				$book->id = $this->db->lastInsertId();
			}
		}catch(PDOException $e){
			echo "Feil ved å legge til ny bok: ". $e->getMessage();
    }
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		try{
			if(empty($_POST['title']) || empty($_POST['author'])){
				$view = new ErrorView();
				$view->create();
				
			}else{
				$sth = $this->db->prepare('UPDATE Book SET title = :t, author = :a, description = :d WHERE id = :id');
				$sth->bindValue(':id', $book->id, PDO::PARAM_STR);
				$sth->bindValue(':t', $book->title, PDO::PARAM_STR);
				$sth->bindValue(':a', $book->author, PDO::PARAM_STR);	
				$sth->bindValue(':d', $book->description, PDO::PARAM_STR);
				$sth->execute();
			}
		}catch(PDOException $e){
			echo "Feil ved å modifisere bok: ". $e->getMessage();
		}	
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try{
			$sth = $this->db->prepare('DELETE FROM Book WHERE id = :id');
			$sth->bindValue(':id', $id, PDO::PARAM_STR);
			$sth->execute();
			
		}catch(PDOException $e){
			echo "Feil ved å slette bok: ". $e->getMessage();
			$view = new ErrorView();
			$view->create();
		}
	}
}

?>